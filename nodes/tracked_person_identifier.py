#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from std_msgs.msg import *
from geometry_msgs.msg import *
import tf
from operator import add
from spencer_tracking_msgs.msg import *
from pyquaternion import Quaternion
from time import sleep
from person_follower.msg import TrackedPersonIdentities
import person_follower
from aether_msgs.msg import *

faces = []
tracks = []
trackHeader = std_msgs.msg.Header()
camPosOdomFrameList = []
camRotOdomFrame = geometry_msgs.msg.Quaternion()

trackedPersonIDs = []
trackedPersonThetas = []
trackedPersonRs = []

faceThetas = []

camera_fx = float('nan')
camera_width = float('nan')

maxMatchingDist = math.radians(15)

matchedTrackIDs = []
matchedTracks = []
matchedFaceNames = []

def init():
	global cmdZGainFace, cmdZOffsetFace, cmdZGainFollow, cmdZOffsetFollow, cmdXGainFollow, followDist, zCmdDeadzone, xCmdDeadzone, cmdZLimit, cmdXLimit

def qv_mult(q1, v1):
	v1 = tf.transformations.unit_vector(v1)
	q2 = list(v1)
	q2.append(0.0)
	return tf.transformations.quaternion_multiply(
		tf.transformations.quaternion_multiply(q1, q2),
		tf.transformations.quaternion_conjugate(q1))

def geoMsgQuat2pyQuat(geoMsgQuat):
	return Quaternion(geoMsgQuat.w,geoMsgQuat.x,geoMsgQuat.y,geoMsgQuat.z)

# tf lookup transform return a list in x,y,z,w format. But pyQuaternion uses w,x,y,z order
def list2pyQuat(listQuat):
	return Quaternion(listQuat[3], listQuat[0], listQuat[1], listQuat[2])

# tf lookup transform return a list in x,y,z,w format.
def list2GeoMsgQuat(listQuat):
	geoMsgQuat = geometry_msgs.msg.Quaternion()
	geoMsgQuat.x = listQuat[0]
	geoMsgQuat.y = listQuat[1]
	geoMsgQuat.z = listQuat[2]
	geoMsgQuat.w = listQuat[3]
	return geoMsgQuat
 
def geoMsgPoint2List(geoMsgPoint):
	return [geoMsgPoint.x, geoMsgPoint.y, geoMsgPoint.z]

def list2GeoMsgPoint(pointList):
	geoMsgPoint = geometry_msgs.msg.Point()
	geoMsgPoint.x = pointList[0]
	geoMsgPoint.y = pointList[1]
	geoMsgPoint.z = pointList[2]
	return geoMsgPoint

# pos is a list, and rot is a pyquaternion.Quaternion
def composeTransforms(pos1, rot1, pos2, rot2):
	pos_out = map(add, rot1.rotate(pos2), pos1)
	rot_out = rot1*rot2
	return (pos_out, rot_out)

# pos is a list, and rot is a pyquaternion.Quaternion
def inverseTransform(pos, rot):
	pos_out_tmp = rot.inverse.rotate(pos)
	pos_out = [-pos_out_tmp[0],-pos_out_tmp[1],-pos_out_tmp[2]]
	rot_out = rot.inverse
	return (pos_out, rot_out)

def callbackCameraInfo(data):
	global camera_fx, camera_width
	camera_fx = data.P[0]
	camera_width = data.width

def callbackFaces(data):
	# rospy.loginfo('============ in callbackFaces =============')
	
	global faces
	faces = data.faces

def callbackTrackedPersons(data):
	# rospy.loginfo('============ in callbackTrackedPersons =============')

	global tracked_person_identities_pub
	global camPosOdomFrameList, camRotOdomFrame, tfl, tfb
	global camera_fx, camera_width
	global faces, faceThetas
	global trackHeader, tracks, trackedPersonIDs, trackedPersonThetas, trackedPersonRs
	global matchedTrackIDs, matchedTracks, matchedFaceNames

	trackHeader = data.header
	tracks = data.tracks
	
	try:
		############## compute each tracked persons (2D) polar coordinates in camera frame

		# get cam pos in odom frame
		# tf returns pos as list x,y,x, and rot as list x,y,z,w
		(camPosOdomFrameList, camRotOdomFrameList) = tfl.lookupTransform('/odom','/camera_color_optical_frame', rospy.Time(0))
		# convert from List data type to Quaternion
		camRotOdomFrame = list2pyQuat(camRotOdomFrameList)

		trackedPersonIDs = []
		trackedPersonThetas = []
		trackedPersonRs = []

		# rospy.loginfo('num tracks: %d', len(data.tracks))
		tracks = data.tracks
		if len(tracks) == 0:
			return
		
		for track in tracks:
			# rospy.loginfo('track id %d', track.track_id)
			# compute odom transform in camera frame 
			(odomPosCamFrameList, odomRotCamFrame) = inverseTransform(camPosOdomFrameList, camRotOdomFrame)
			
			trackPosOdomFrameList = geoMsgPoint2List(track.pose.pose.position)
			trackRotOdomFrame = geoMsgQuat2pyQuat(track.pose.pose.orientation)
			# compute track transform in camera frame (trackInCamFrame = OdomInCamFrame * trackInCamFrame)
			(trackPosCamFrameList, trackRotCamFrame) = composeTransforms(odomPosCamFrameList, odomRotCamFrame, trackPosOdomFrameList, trackRotOdomFrame)

			# rospy.loginfo('trackPosCamFrame %f %f %f', trackPosCamFrameList[0], trackPosCamFrameList[1], trackPosCamFrameList[2])
			
			radius = math.sqrt(trackPosCamFrameList[0]**2 + trackPosCamFrameList[2]**2) # in 2D (ground) plane only
			theta = math.atan2(trackPosCamFrameList[0],trackPosCamFrameList[2])
			trackedPersonRs.append(radius)
			trackedPersonThetas.append(theta)
			trackedPersonIDs.append(track.track_id)
			# rospy.loginfo('track_id ,r, theta: %d %f %f', track.track_id, radius, math.degrees(theta))

		############## computer each recognized face's theta angle in camera frame

		faceThetas = []
		
		for face in faces:
			face_x = ((face.bounding_box[0] + face.bounding_box[2]) - camera_width) / 2
			# rospy.loginfo('face_x %f' ,face_x)
			# rospy.loginfo('camera_fx %f' ,camera_fx)
			faceTheta = math.atan2(face_x, camera_fx)
			# rospy.loginfo('face, theta: %s %f', face.name, math.degrees(faceTheta));
			faceThetas.append(faceTheta)

			
		############### match faces to tracked persons
		# rospy.loginfo('========= matching faces to tracked persons')
		
		trackIndex = 0
		for track in tracks:
			closestFace = None
			faceIndex = 0
			minDist = 10000000
			# rospy.loginfo('==== track id %d ====', track.track_id)

			#### find closest face to this track
			for face in faces:
				distance = abs(faceThetas[faceIndex]-trackedPersonThetas[trackIndex])
				# rospy.loginfo('face, dist: %s %f', face.name, distance)
				if (distance < maxMatchingDist) and (distance < minDist):
					closestFace = face.name
					minDist = distance					
				faceIndex = faceIndex + 1
			# rospy.loginfo('closestFace %s', closestFace)

			## if found a match to a track_id for this face
			if closestFace != None:
				
				## if both this face and track_id are in the lists but are not associated with eachother,
				## remove the exisitng entry pair of the track_id in preparation for the next block of code,
				## where the existing face is updated with this track_id.
				## This prevents multiple entries with same track_id but different names.
				try: 
					if matchedFaceNames.index(closestFace) != matchedTrackIDs.index(track.track_id):
						matchedFaceNames.pop(matchedTrackIDs.index(track.track_id))
						matchedTracks.pop(matchedTrackIDs.index(track.track_id))
						matchedTrackIDs.remove(track.track_id)

				except ValueError:
					foo = 1 # do nothing
					
				try: ## if this face is attached to an old track_id, update the old track_id
					matchedTrackIDs[matchedFaceNames.index(closestFace)] = track.track_id
					matchedTracks[matchedFaceNames.index(closestFace)] = track
				except ValueError: ## else if there is no track_id associated to this face
					try: # if this track_id already exists in matchedTrackIDs, update thenn face associated with it
						matchedFaceNames[matchedTrackIDs.index(track.track_id)] = closestFace
					except ValueError: ## else add new entry
						matchedTrackIDs.append(track.track_id)
						matchedTracks.append(track)
						matchedFaceNames.append(closestFace)
					
			trackIndex = trackIndex + 1

			
			
	except Exception as err:
		rospy.loginfo('exception: %s', err)


def identify_tracked_persons():
	global tracked_person_identities_pub, tfl, tfb
	global trackHeader, tracks, faces
	global matchedTrackIDs, matchedTracks, matchedFaceNames
	
	rospy.init_node('tracked_persons_identifier', anonymous=True)
	init()
	
	tfl = tf.TransformListener()
	tfb = tf.TransformBroadcaster()
	
	tracked_person_identities_pub = rospy.Publisher('/tracked_person_identities', person_follower.msg.TrackedPersonIdentities, queue_size=1)
	rospy.Subscriber("/spencer/perception/tracked_persons_confirmed_by_HOG_or_upper_body", spencer_tracking_msgs.msg.TrackedPersons, callbackTrackedPersons, queue_size=1)
	# rospy.Subscriber("/spencer/perception/tracked_persons", spencer_tracking_msgs.msg.TrackedPersons, callbackTrackedPersons)
	# rospy.Subscriber("/spencer/perception_internal/detected_persons/laser_front", spencer_tracking_msgs.msg.TrackedPersons, callbackTrackedPersons)
	rospy.Subscriber("/face_detection/face_identities", aether_msgs.msg.Faces, callbackFaces, queue_size=1)
	rospy.Subscriber("/camera/color/camera_info", sensor_msgs.msg.CameraInfo, callbackCameraInfo, queue_size=1)

	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():

		rospy.loginfo('loop')
		# print 'matchedTrackIDs'
		# print matchedTrackIDs
		# print 'matchedFaceNames'
		# print matchedFaceNames

		######## remove tracks that no longer exist

		for matchedTrackID in matchedTrackIDs:
			trackIDExists = False
			for track in tracks:
				if matchedTrackID == track.track_id:
					trackIDExists = True
			if not trackIDExists: ## remove non existing tracks
				matchedFaceNames.pop(matchedTrackIDs.index(matchedTrackID))
				matchedTracks.pop(matchedTrackIDs.index(matchedTrackID))
				matchedTrackIDs.remove(matchedTrackID)
			

		############## publish matching results

		trackedPersonIdentities = person_follower.msg.TrackedPersonIdentities()
		trackedPersonIdentities.header = trackHeader

		index = 0
		print matchedFaceNames
		for track in matchedTracks:
			trackedPersonIdentity = person_follower.msg.TrackedPersonIdentity()
			trackedPersonIdentity.name = matchedFaceNames[index]
			trackedPersonIdentity.trackedPerson = track
			trackedPersonIdentities.trackedPersonIdentities.append(trackedPersonIdentity)
			index = index + 1
			
		tracked_person_identities_pub.publish(trackedPersonIdentities)

		
		rate.sleep()

	rospy.loginfo('node shutdown')
		
if __name__ == '__main__':
	try:
		identify_tracked_persons()
	except rospy.ROSInterruptException:
		pass

	
