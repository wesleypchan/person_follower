#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from std_msgs.msg import String
from geometry_msgs.msg import *
import tf
from operator import add
from spencer_tracking_msgs.msg import *
from pyquaternion import Quaternion
from time import sleep
from person_follower.msg import TrackedPersonIdentities
from person_follower.msg import PersonFollowerCmd
from sensor_msgs.msg import LaserScan

## Modes:
# STAY - doesn't do anything
# LOOKAT - turns to look at the target person 
# SHADOW - tries to maintain a fixed distance from the target person
# FOLLOW - tries to follow the target person, just like SHADOW mode, but allows the person to approach and touch the robot
# CHASE - lets the target person chase the robot by keeping a fixed distance from the person, but will not follow the person if person walks away


mode = 'STAY'
person_facer_pub = []
person_follower_pub = []
target_person_pub = []
move_base_goal_pub = []
tracks = []
distances = []
robotPosOdomFrameList = []
robotRotOdomFrame = geometry_msgs.msg.Quaternion()
isObstacleFront = False
isObstacleBack = False

targetName = 'anybody'
target = None

def init():
	global cmdZGainLookat, cmdZOffsetLookat
	global cmdZGainShadow, cmdZOffsetShadow, cmdXGainShadow, shadowDist
	global cmdZGainFollow, cmdZOffsetFollow, cmdXGainFollow, followDist
	global cmdZGainChase, cmdZOffsetChase, cmdXGainChase, chaseDist
	global zCmdDeadzone, xCmdDeadzone, cmdZLimit, cmdXLimit
	
	# gains for different modes
	cmdZGainLookat = rospy.get_param('person_follower/cmdZGainLookat', 1.5)
	cmdZOffsetLookat = rospy.get_param('person_follower/cmdZOffsetLookat', 0)
	cmdZGainShadow = rospy.get_param('person_follower/cmdZGainShadow', 1.5)
	cmdZOffsetShadow = rospy.get_param('person_follower/cmdZOffsetShadow', 0)
	cmdXGainShadow = rospy.get_param('person_follower/cmdXGainShadow', 1.5)
	cmdZGainFollow = rospy.get_param('person_follower/cmdZGainFollow', 1.5)
	cmdZOffsetFollow = rospy.get_param('person_follower/cmdZOffsetFollow', 0)
	cmdXGainFollow = rospy.get_param('person_follower/cmdXGainFollow', 1.5)
	cmdZGainChase = rospy.get_param('person_follower/cmdZGainChase', 1.5)
	cmdZOffsetChase = rospy.get_param('person_follower/cmdZOffsetChase', 0)
	cmdXGainChase = rospy.get_param('person_follower/cmdXGainChase', 1.5)

	# distance for shadowing person
	shadowDist = rospy.get_param('person_follower/shadowDist', 1.0)

	# distance for following person
	followDist = rospy.get_param('person_follower/followDist', 1.0)

	# distance for being chased by a person
	chaseDist = rospy.get_param('person_follower/chaseDist', 1.5)

	# deadzone for different modes
	zCmdDeadzone = rospy.get_param('person_follower/zCmdDeadzone', 0.15)
	xCmdDeadzone = rospy.get_param('person_follower/xCmdDeadzone', 0.1)
	
	cmdZLimit = rospy.get_param('person_follower/cmdZLimit', 1.5)
	cmdXLimit = rospy.get_param('person_follower/cmdXLimit', 2.0)

def qv_mult(q1, v1):
	v1 = tf.transformations.unit_vector(v1)
	q2 = list(v1)
	q2.append(0.0)
	return tf.transformations.quaternion_multiply(
		tf.transformations.quaternion_multiply(q1, q2),
		tf.transformations.quaternion_conjugate(q1))

def geoMsgQuat2pyQuat(geoMsgQuat):
	return Quaternion(geoMsgQuat.w,geoMsgQuat.x,geoMsgQuat.y,geoMsgQuat.z)

# tf lookup transform return a list in x,y,z,w format. But pyQuaternion uses w,x,y,z order
def list2pyQuat(listQuat):
	return Quaternion(listQuat[3], listQuat[0], listQuat[1], listQuat[2])

# tf lookup transform return a list in x,y,z,w format.
def list2GeoMsgQuat(listQuat):
	geoMsgQuat = geometry_msgs.msg.Quaternion()
	geoMsgQuat.x = listQuat[0]
	geoMsgQuat.y = listQuat[1]
	geoMsgQuat.z = listQuat[2]
	geoMsgQuat.w = listQuat[3]
	return geoMsgQuat
 
def geoMsgPoint2List(geoMsgPoint):
	return [geoMsgPoint.x, geoMsgPoint.y, geoMsgPoint.z]

def list2GeoMsgPoint(pointList):
	geoMsgPoint = geometry_msgs.msg.Point()
	geoMsgPoint.x = pointList[0]
	geoMsgPoint.y = pointList[1]
	geoMsgPoint.z = pointList[2]
	return geoMsgPoint

# pos is a list, and rot is a pyquaternion.Quaternion
def composeTransforms(pos1, rot1, pos2, rot2):
	pos_out = map(add, rot1.rotate(pos2), pos1)
	rot_out = rot1*rot2
	return (pos_out, rot_out)

# pos is a list, and rot is a pyquaternion.Quaternion
def inverseTransform(pos, rot):
	pos_out_tmp = rot.inverse.rotate(pos)
	pos_out = [-pos_out_tmp[0],-pos_out_tmp[1],-pos_out_tmp[2]]
	rot_out = rot.inverse
	return (pos_out, rot_out)

def getClosestPersonPoint(tracks):
	
	# rospy.loginfo('============ In getClosestPersonPoint =============')
    # get robot pos in odom frame
    # tf returns pos as list x,y,x, and rot as list x,y,z,w
	(robotPosOdomFrameList, robotRotOdomFrameList) = tfl.lookupTransform('/odom','/base_link', rospy.Time(0))
	# convert from List data type to Quaternion
	robotRotOdomFrame = list2pyQuat(robotRotOdomFrameList)

	distances = []
	# rospy.loginfo('num tracks: %d', len(tracks))
	if len(tracks) == 0:
		return []
	
	index = 0
	minDist = 10000000
	minDistIndex = -1
	closestTrackPosRobotFrame = []
	closestTrackRotRobotFrame = []
	
	for track in tracks:
		# rospy.loginfo('track id %d', track.track_id)
		# compute odom transform in robot frame 
		(odomPosRobotFrameList, odomRotRobotFrame) = inverseTransform(robotPosOdomFrameList, robotRotOdomFrame)
		
		trackPosOdomFrameList = geoMsgPoint2List(track.pose.pose.position)
		trackRotOdomFrame = geoMsgQuat2pyQuat(track.pose.pose.orientation)
		# compute track transform in robot frame (trackInRobotFrame = OdomInRobotFrame * trackInOdomFrame)
		(trackPosRobotFrameList, trackRotRobotFrame) = composeTransforms(odomPosRobotFrameList, odomRotRobotFrame, trackPosOdomFrameList, trackRotOdomFrame)
			
		distance = math.sqrt(trackPosRobotFrameList[0]**2 + trackPosRobotFrameList[1]**2 + trackPosRobotFrameList[2]**2)
		# rospy.loginfo('distance %f', distance)
		distances.append(distance)
		
		# remember the closest track
		if distance < minDist:
			minDist = distance
			minDistIndex = index
			closestTrackPosRobotFrame = trackPosRobotFrameList
			closestTrackRotRobotFrame = trackRotRobotFrame
			is_target_matched = track.is_matched

		index = index+1
		
	return closestTrackPosRobotFrame, is_target_matched

def getTargetPersonPoint(targetID, tracks):
	
	# rospy.loginfo('============ In getTargetPersonPoint =============')
    # get robot pos in odom frame
    # tf returns pos as list x,y,x, and rot as list x,y,z,w
	(robotPosOdomFrameList, robotRotOdomFrameList) = tfl.lookupTransform('/odom','/base_link', rospy.Time(0))
	# convert from List data type to Quaternion
	robotRotOdomFrame = list2pyQuat(robotRotOdomFrameList)

	# rospy.loginfo('num tracks: %d', len(tracks))
	if len(tracks) == 0:
		return []
	
	targetTrackPosRobotFrame = []
	targetTrackRotRobotFrame = []

	# if targetID == []:		
		# return []

	for track in tracks:
		if track.track_id == targetID:
			# rospy.loginfo('track id %d found', track.track_id)
			# compute odom transform in robot frame 
			(odomPosRobotFrameList, odomRotRobotFrame) = inverseTransform(robotPosOdomFrameList, robotRotOdomFrame)
		
			trackPosOdomFrameList = geoMsgPoint2List(track.pose.pose.position)
			trackRotOdomFrame = geoMsgQuat2pyQuat(track.pose.pose.orientation)
			# compute track transform in robot frame (trackInRobotFrame = OdomInRobotFrame * trackInOdomFrame)
			(trackPosRobotFrameList, trackRotRobotFrame) = composeTransforms(odomPosRobotFrameList, odomRotRobotFrame, trackPosOdomFrameList, trackRotOdomFrame)

			targetTrackPosRobotFrame = trackPosRobotFrameList
			targetTrackRotRobotFrame = trackRotRobotFrame
			is_target_matched = track.is_matched
					
	return targetTrackPosRobotFrame, is_target_matched


def callbackCommand(data):
	# rospy.loginfo('============ in callbackCommand =============')
	global mode, targetName
	mode = data.mode
	targetName = data.target
	rospy.loginfo('mode and targetName set to %s %s', mode, targetName)

def callbackIdentities(data):
	# rospy.loginfo('============ in callbackIdentities =============')
	global targetName, target
	target = None
	for identity in data.trackedPersonIdentities:
		if identity.name.lower() == targetName.lower():
			target = identity.trackedPerson
	# rospy.loginfo('target set to track with track_id %d', target.track_id)

def callbackScan(data):
	global isObstacleFront, isObstacleBack
	rospy.loginfo('============ in callbackLaser =============')

	pointCountFront = 0
	pointCountBack = 0
	x_min_front = 0 
	x_max_front = 1.0 # how close an object in front has to be before robot stops when going forward
	y_min_front = -0.25
	y_max_front = 0.25
	
	x_min_back = -0.5
	x_max_back = 0
	y_min_back = -0.25 # how close an object behind has to be before robot stops when going backward
	y_max_back = 0.25
	pointsThres = 2

	theta = data.angle_min
	
	for pointDist in data.ranges:
		# x points straigth backward out of the laser sensor (zero deg is back)
		x_coord = pointDist * -math.cos(theta)
		y_coord = pointDist * math.sin(theta)
		if (x_coord > x_min_front) and (x_coord < x_max_front) and (y_coord > y_min_front) and (y_coord < y_max_front):
			pointCountFront=pointCountFront+1
			# rospy.loginfo("pointDist theta x y %f %f %f %f front", pointDist, math.degrees(theta), x_coord, y_coord)
		elif (x_coord > x_min_back) and (x_coord < x_max_back) and (y_coord > y_min_back) and (y_coord < y_max_back):
			pointCountBack=pointCountBack+1
			# rospy.loginfo("pointDist theta x y %f %f %f %f back", pointDist, math.degrees(theta), x_coord, y_coord)
		theta = theta + data.angle_increment


	rospy.loginfo("pointCountFront %f", pointCountFront)
	rospy.loginfo("pointCountBack %f", pointCountBack)
	if pointCountFront > pointsThres:
		isObstacleFront = True
		rospy.loginfo("isObstacleFront true")
	else:
		isObstacleFront = False
		rospy.loginfo("isObstacleFront false")

	if pointCountBack > pointsThres:
		isObstacleBack = True
		rospy.loginfo("isObstacleBack true")
	else:
		isObstacleBack = False
		rospy.loginfo("isObstacleBack false")


		
def callbackTrackedPersons(data):
	global isObstacleFront, isObstacleBack
	# rospy.loginfo('============ in callbackTrackedPersons =============')
	# rospy.loginfo('mode and targetName set to %s %s', mode, targetName)
	
	if mode == 'STAY':
		return
	
	global person_facer_pub, person_follower_pub, target_person_pub, move_base_goal_pub
	global tracks, distances, robotPosOdomFrameList, robotRotOdomFrame, tfl, tfb
	global cmdZGainLookat, cmdZOffsetLookat
	global cmdZGainShadow, cmdZOffsetShadow, cmdXGainShadow, shadowDist
	global cmdZGainFollow, cmdZOffsetFollow, cmdXGainFollow, followDist
	global cmdZGainChase, cmdZOffsetChase, cmdXGainChase, chaseDist
	global zCmdDeadzone, xCmdDeadzone, cmdZLimit, cmdXLimit
	global target

	tracks = data.tracks
	
	try:

		if target == None:
			return
		
		if (targetName.lower() == 'anyone') or (targetName.lower() == 'anybody'):
			targetTrackPosRobotFrame, is_target_matched = getClosestPersonPoint(data.tracks)
		else:
			targetTrackPosRobotFrame, is_target_matched = getTargetPersonPoint(target.track_id, data.tracks)

		####### computer and publish move base goal in map frame ######
		# publish position of closet person in robot frame
		targetPersonPointStamped = geometry_msgs.msg.PointStamped()
		targetPersonPoint = geometry_msgs.msg.Point()
	
		if targetTrackPosRobotFrame != []:
			# rospy.loginfo('targetTrackPosRobotFrame %f %f %f', targetTrackPosRobotFrame[0], targetTrackPosRobotFrame[1], targetTrackPosRobotFrame[2])
			targetPersonPoint.x = targetTrackPosRobotFrame[0]
			targetPersonPoint.y = targetTrackPosRobotFrame[1]
			targetPersonPoint.z = targetTrackPosRobotFrame[2]
		else:
			rospy.loginfo('targetTrackPosRobotFrame empty!')
			targetPersonPoint = geometry_msgs.msg.Point()
			targetPersonPoint.x = float('nan')
			targetPersonPoint.y = float('nan')
			targetPersonPoint.z = float('nan')
			return
			
		targetPersonPointStamped.header.frame_id = "base_link"
		targetPersonPointStamped.point = targetPersonPoint
		target_person_pub.publish(targetPersonPointStamped)
		# rospy.loginfo('targetPersonPoint.x: %f', targetPersonPoint.x)
		
		# compute the goal pos as 1 meter before the person
		targetPersonPointDist = math.sqrt(targetPersonPoint.x**2 + targetPersonPoint.y**2 + targetPersonPoint.z**2)
		goalPoint = geometry_msgs.msg.Point()
		goalPoint.x = targetPersonPoint.x * (targetPersonPointDist - 1.0) / targetPersonPointDist
		goalPoint.y = targetPersonPoint.y * (targetPersonPointDist - 1.0) / targetPersonPointDist
		goalPoint.z = targetPersonPoint.z * (targetPersonPointDist - 1.0) / targetPersonPointDist
		
		tfb.sendTransform(geoMsgPoint2List(goalPoint),
						  tf.transformations.quaternion_from_euler(0, 0, math.atan2(targetPersonPoint.y,targetPersonPoint.x)),
						  rospy.Time.now(),
						  "follow_goal",
						  "base_link")
	
		# tf returns pos as list x,y,x, and rot as list x,y,z,w
		(goalPosMapFrameList, goalRotMapFrameList) = tfl.lookupTransform('/map','/follow_goal', rospy.Time(0))
		goalMsg = geometry_msgs.msg.PoseStamped()
		goalMsg.header.stamp = rospy.get_rostime()
		goalMsg.header.frame_id = '/map'
		goalMsg.pose.position = list2GeoMsgPoint(goalPosMapFrameList)
		goalMsg.pose.orientation = list2GeoMsgQuat(goalRotMapFrameList)
		# move_base_goal_pub.publish(goalMsg)
		# sleep(2)
		
		####### publish following and facing cmd_vel ######
		xCmdVel = 0
		zCmdVel = 0
		cmdVel = geometry_msgs.msg.Twist()
		if targetTrackPosRobotFrame != []:
			targetPersonTheta = math.atan2(targetPersonPoint.y,targetPersonPoint.x)
			# rospy.loginfo('targetPersonTheta: %f', targetPersonTheta)

			if mode == 'SHADOW':
				# if (abs(targetPersonPoint.x - shadowDist) > xCmdDeadzone) and is_target_matched: # check larger than deadzone, and is matched
				if (abs(targetPersonPoint.x - shadowDist) > xCmdDeadzone): # check larger than deadzone
					xCmdVel = (targetPersonPoint.x - shadowDist) * cmdXGainShadow

					# check and apply limit					
					if xCmdVel > cmdXLimit:
						xCmdVel = cmdXLimit
					elif xCmdVel < 0 :
						xCmdVel = 0

					# rospy.loginfo('zCmdDeadzone: %f', zCmdDeadzone)
				if abs(targetPersonTheta) > zCmdDeadzone: # check larger than deadzone
					if targetPersonTheta > 0:
						zCmdVel = targetPersonTheta * cmdZGainShadow + cmdZOffsetShadow
					else:
						zCmdVel = targetPersonTheta * cmdZGainShadow - cmdZOffsetShadow
					# check and apply limit

					if zCmdVel > cmdZLimit:
						zCmdVel = cmdZLimit
					elif zCmdVel < -cmdZLimit:
						zCmdVel = -cmdZLimit
									
				cmdVel.linear.x = xCmdVel
				cmdVel.angular.z = zCmdVel
			
			if mode == 'FOLLOW':
				# if (abs(targetPersonPoint.x - followDist) > xCmdDeadzone) and is_target_matched: # check larger than deadzone, and is matched
				if (abs(targetPersonPoint.x - followDist) > xCmdDeadzone): # check larger than deadzone
					xCmdVel = (targetPersonPoint.x - followDist) * cmdXGainFollow

					# check and apply limit					
					if xCmdVel > cmdXLimit:
						xCmdVel = cmdXLimit
					elif xCmdVel < 0 :
						xCmdVel = 0

					# rospy.loginfo('zCmdDeadzone: %f', zCmdDeadzone)
				if abs(targetPersonTheta) > zCmdDeadzone: # check larger than deadzone
					if targetPersonTheta > 0:
						zCmdVel = targetPersonTheta * cmdZGainFollow + cmdZOffsetFollow
					else:
						zCmdVel = targetPersonTheta * cmdZGainFollow - cmdZOffsetFollow
					# check and apply limit

					if zCmdVel > cmdZLimit:
						zCmdVel = cmdZLimit
					elif zCmdVel < -cmdZLimit:
						zCmdVel = -cmdZLimit
									
				cmdVel.linear.x = xCmdVel
				cmdVel.angular.z = zCmdVel

			if mode == 'CHASE':
				if abs(targetPersonPoint.x - chaseDist) > xCmdDeadzone: # check larger than deadzone
					xCmdVel = (targetPersonPoint.x - chaseDist) * cmdXGainChase

					if xCmdVel > 0:
						xCmdVel = 0
					elif xCmdVel < -cmdXLimit :
						xCmdVel = -cmdXLimit

				if abs(targetPersonTheta) > zCmdDeadzone: # check larger than deadzone
					if targetPersonTheta > 0:
						zCmdVel = targetPersonTheta * cmdZGainChase + cmdZOffsetChase
					else:
						zCmdVel = targetPersonTheta * cmdZGainChase - cmdZOffsetChase
					# check and apply limit

					if zCmdVel > cmdZLimit:
						zCmdVel = cmdZLimit
					elif zCmdVel < -cmdZLimit:
						zCmdVel = -cmdZLimit
									
				cmdVel.linear.x = xCmdVel
				cmdVel.angular.z = zCmdVel
				
			elif mode == 'LOOKAT':
				if abs(targetPersonTheta) > zCmdDeadzone:
					if targetPersonTheta > 0:
						zCmdVel = targetPersonTheta * cmdZGainLookat + cmdZOffsetLookat
					else:
						zCmdVel = targetPersonTheta * cmdZGainLookat - cmdZOffsetLookat
					# check and apply limit
					if zCmdVel > cmdZLimit:
						zCmdVel = cmdZLimit
					elif zCmdVel < -cmdZLimit:
						zCmdVel = -cmdZLimit

				cmdVel.angular.z = zCmdVel

			# if obstacled detected don't move into it
			if isObstacleFront and (cmdVel.linear.x > 0):
				cmdVel.linear.x = 0
			elif isObstacleBack and (cmdVel.linear.x < 0):
				cmdVel.linear.x = 0
				
			person_follower_pub.publish(cmdVel)
			rospy.loginfo('cmdVel x z %f %f', cmdVel.linear.x, cmdVel.angular.z);

				
	except Exception as err:
		rospy.loginfo('exception: %s', err)
		
	
def follow_person():
	global person_follower_pub, target_person_pub, move_base_goal_pub
	global tracks, distances, robotPosOdomFrameList, robotRotOdomFrame, tfl, tfb
	
	rospy.init_node('spencer_person_follower', anonymous=True)
	init()
	
	tfl = tf.TransformListener()
	tfb = tf.TransformBroadcaster()

	
	person_follower_pub = rospy.Publisher('/person_follower/cmd_vel', Twist, queue_size=1)
	target_person_pub = rospy.Publisher('/person_follower/closestPerson', PointStamped, queue_size=1)
	move_base_goal_pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=1)
	rospy.Subscriber("/spencer/perception/tracked_persons_confirmed_by_HOG_or_upper_body", TrackedPersons, callbackTrackedPersons)
	# rospy.Subscriber("/spencer/perception/tracked_persons", spencer_tracking_msgs.msg.TrackedPersons, callbackTrackedPersons)
	# rospy.Subscriber("/spencer/perception_internal/detected_persons/laser_front", spencer_tracking_msgs.msg.TrackedPersons, callbackTrackedPersons)
	rospy.Subscriber("/person_follower/command", PersonFollowerCmd, callbackCommand)
	rospy.Subscriber("/tracked_person_identities", TrackedPersonIdentities, callbackIdentities)
	rospy.Subscriber("/scan", LaserScan, callbackScan)

	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		
		rate.sleep()

	rospy.loginfo('node shutdown')
		
if __name__ == '__main__':
	try:
		follow_person()
	except rospy.ROSInterruptException:
		pass
