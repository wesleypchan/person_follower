#!/usr/bin/env roseus

;; A simple blob follower


(ros::load-ros-manifest "sensor_msgs")

;; finds a blob of laser points in front and publish cmd_vel to follow it
(defun callback (data)
    
    (setq x_min 0.010)
    (setq x_max 1.8)
    (setq y_min -0.40)
    (setq y_max 0.40)

    (setq zGainFace -1.5) ;; negative since the SICK laser is mounted upside down on powerbot
                
    (setq zGainFollow -1.5) ;; negative since the SICK laser is mounted upside down on powerbot
    (setq xGainFollow 0.5)
    (setq followDist 1.0)

    (setq points (send data :ranges))
    (setq angle_min (send data :angle_min))
    (setq angle_inc (send data :angle_increment))
    
    (setq index 0)
    (setq numPoints (length points))
    

    (setq ptCount 0)
    (setq sumx 0)
    (setq sumy 0)

    (setq nan (elt #f(nan) 0))
    
    (while (< index numPoints)

        ;; convert r theta to x y
        ;; x points straight forward out of the laser sensor
        (setq theta (+ angle_min (* index angle_inc)))
        (setq dist (elt points index))
        (setq x_coord (* dist (cos theta)))
        (setq y_coord (* dist (sin theta)))

        ;; if point is within max/min x/y add to sum
        (if (and (not (eq dist nan))
                 (< x_coord x_max) (> x_coord x_min)
                 (< y_coord y_max) (> y_coord y_min))
                (progn
                    (setq sumx (+ sumx x_coord))
                    (setq sumy (+ sumy y_coord))
                    (setq ptCount (+ ptCount 1))
                    ;; (format t "theta ~a dist ~a x_coord ~a y_coord ~a~%" theta dist x_coord y_coord)
                    )
            )
        
        (setq index (+ index 1))
        )

    (setq velCmdFace (instance geometry_msgs::Twist :init))
    (setq velCmdFollow (instance geometry_msgs::Twist :init))
    
    (if (< ptCount 3)
            (progn
                (setq avgx nan)
                (setq avgy nan)
                )
        (progn
            (setq avgx (/ sumx ptCount))   
            (setq avgy (/ sumy ptCount))
           
            (setq zCmdFace (* avgy zGainFace))
            ;; dead zone
            (setq zCmdMinFace 0.03)
            (if (< (abs zCmdFace) zCmdMinFace)
                    (setq zCmdFace 0))
            (send velCmdFace :angular :z zCmdFace)
                


            ;; dead zone
            (setq zCmdMinFollow 0.03)
            (setq xCmdMinFollow 0.1)
            (setq zCmdFollow (* avgy zGainFollow))
            (if (< (abs zCmdFollow) zCmdMinFollow)
                    (setq zCmdFollow 0))
            (setq xCmdFollow (* (- avgx followDist) xGainFollow))
            (if (< (abs xCmdFollow) xCmdMinFollow)
                    (setq xCmdFollow 0))

            (send velCmdFollow :angular :z zCmdFollow)
            (send velCmdFollow :linear :x xCmdFollow)


            )
        )

    (ros::publish "person_facer/cmd_vel" velCmdFace)
    (ros::publish "person_follower/cmd_vel" velCmdFollow)
        
    ;; (format t "avg: ~a, ~a ~%" avgx avgy)
    (format t "avg: ~a, ~a ptCount ~a~%" avgx avgy ptCount)
    )


(ros::roseus "person_follower")

(ros::subscribe "scan"
                sensor_msgs::LaserScan
                #'callback 10)
(ros::advertise "person_follower/cmd_vel" geometry_msgs::Twist)
(ros::advertise "person_facer/cmd_vel" geometry_msgs::Twist)

(ros::rate 1000)
(while (ros::ok)
    (ros::spin-once)
    (ros::sleep)
    )
