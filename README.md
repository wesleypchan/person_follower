# person_follower

This package provides the functionality of following a person and some other related functionalities based on results from a people tracker.

If face recognigtion is available, a separate node is provided to identify each tracked person from the people tracker with a name.

Currently, this package is implemented using the [spencer people tracker] (https://github.com/spencer-project/spencer_people_tracking) and the face recognizer from the [jdq_3sr_aether_vison package] (https://github.com/3SpheresRoboticsProject/jdq_3sr_aether_server/tree/master/jdq_3sr_aether_vision). If another people tracker or face recognition package is to be used, simply provided the same message type inputs to the topics subscribed to by this package (documented below).

## Running

Person follower:
''' rosrun person_follower spencer_person_follower.py'''

Tracked person identifier (if face recognition available):
''' rosrun person_follower tracked_person_identifier.py'''

## Avaliable modes

STAY - does nothing

LOOKAT - turns to continuously look at the target person, while standing at the same spot.

SHADOW - tries to maintain a set distacne from the target person

FOLLOW - similar to SHADOW mode, follow the target person if he/she move farther than a set distance away from the robot. But the person can approace the robot and it will not back away. 

CHASE - allows the target person to chase the robot. Tries to keep a minimum set distance away from the target person.

## Issuing commands

To issue a command, publish the desired mode and target name into the topic /person_follower_controller/command

e.g. $ rostopic pub /person_follower_controller/command std_msgs/String "{mode: 'FOLLOW', target: 'Jon'}"

If face detection is not available, or if you do not want to specify a target, publish "anyone" for the target name.

For STAY mode, the target field is ignored.

## Published topics

- person_follower/cmd_vel(geometry_msgs/Twist)
Velocity command for looking at, shadowing, following, etc. the target person. To be sent to the robot.

- person_follower/targetPerson(geometry_msgs/PointStamped)
Location of the target person. Used primarily for visualization purposes.

- tracked_person_identities(person_follower/TrackedPersonIdentities)
Associated pairs of tracked person and recognized face. For identifying the name of a tracked person.

## Subscribed topics

- person_follower/command(person_follower/PersonFollowerCmd)
Input topic used to set the mode and target person for the person follower. 

- spencer/perception/tracked_persons_confirmed_by_HOG_or_upper_body(spencer_tracking_msgs/TrackedPersons)
Tracked person information. Contains the 2D coordinates of each tracked person. Coordinates are assumed to be given in the Odom frame. Each tracked person is identified by the unique track_id field in the message.

- tracked_person_identities(person_follower/TrackedPersonIdentities)
Topic containing entires that associate tracked persons with names. This topic can be provided by the tracked_person_identifier node, which uses face recognition, in the same package, or by another package that provides equivalent functionality. 

- scan(sensor_msgs/LaserScan)
Laser scan topic used for obstacle detection and stopping the robot from running into things. It is assummed that 0 deg of the scan is aligned to point directly backwards relative to the robot.

- camera/color/camera_info(sensor_msgs_camera_info)
The camera info of the input image to the face recognizer.

- face_detection/face_identities(aether_msgs/Faces)
Face recognition results.

## Parameters 

A set of parameters are provided for tuning the behaviour of the robot

For all modes:

- person_follower/cmdZLimit 
Angular velocity limit.

- person_follower/cmdXLimit 
Angular linear limit.

- person_follower/zCmdDeadzone
Angular deadzone within which if the target stands the robot will not try to change its heading.

- person_follower/xCmdDeadzone
Tolerance around the set distance, within which if the target stands the robot will not try to change its position.

For LOOKAT mode:
- person_follower/cmdZGainLookat
Angular velocity gain for LOOKAT mode.

- person_follower/cmdZOffsetLookat
Angular velocity offset for LOOKAT mode. For overcoming high turning friction.

For SHADOW mode:

- person_follower/shadowDist
Set distance for SHADOW mode

- person_follower/cmdZGainShadow
Angular velocity gain for SHADOW mode.

- person_follower/cmdZOffsetShadow
Angular velocity offset for SHADOW mode. For overcoming high turning friction.

- person_follower/cmdXGainShadow
Linear velocity gain for SHADOW mode.

For FOLLOW mode:

- person_follower/followDist
Set distance for FOLLOW mode

- person_follower/cmdZGainFollow
Angular velocity gain for FOLLOW mode.

- person_follower/cmdZOffsetFollow
Angular velocity offset for FOLLOW mode. For overcoming high turning friction.

- person_follower/cmdXGainFollow
Linear velocity gain for FOLLOW mode.

For CHASE mode:

- person_follower/chaseDist
Set distance for CHASE mode

- person_follower/cmdZGainChase
Angular velocity gain for CHASE mode.

- person_follower/cmdZOffsetChase
Angular velocity offset for Chase mode. For overcoming high turning friction.

- person_follower/cmdXGainChase
Linear velocity gain for CHASE mode.

## Messages

- PersonFollowerCmd 
Contains the fields mode and target. The mode specifies what mode to put the robot in, and the target specifies the name of the target person.

- TrackedPersonIdentitiy
Contains a name field and a trackedPerson field. This is used to associate the name of a recognized person to a trackedPerson object returned by the people tracker

- TrackedPersonIdentities
Contains a header field and an array of TrackedPersonIdentities. This holds all the associated name-trackedPerson pairs.
